import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { BattleField } from './components/BattleField/BattleField.tsx';

import './App.sass';

const App = () => {
    return (
        <Fragment>
            <header>
                <span>GrApp</span>
            </header>
            <main>
                <span className='image'></span>
                <BattleField/>
            </main>
        </Fragment>
    )
}

ReactDOM.render(<App />, document.getElementById('App'));
