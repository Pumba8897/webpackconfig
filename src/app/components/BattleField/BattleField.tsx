import * as React from "react";
// import './canvas';

// export interface BattleFieldProps { compiler: string; framework: string; }

export const BattleField = () => {
    let canvasRef = React.useRef();

    React.useEffect(() => {
        const canvasNode: any = canvasRef.current;
        const ctx = canvasNode.getContext('2d');
        const canvasW: number = 300;
        const canvasH: number = 300;
        canvasNode.width = canvasW;
        canvasNode.height = canvasH;
        ctx.lineWidth = 1;
        for(let i: number =20; i < canvasW; i+=20) {
            ctx.moveTo(i, 0);
            ctx.lineTo(i, canvasH);
            ctx.stroke();
        }
        for(let i: number =20; i < canvasH; i+=20) {
            ctx.moveTo(0, i);
            ctx.lineTo(canvasW, i);
            ctx.stroke();
        }
    })


    return(
        <div>
            <canvas ref={canvasRef} id='battleField' ></canvas>
        </div>
    )
};